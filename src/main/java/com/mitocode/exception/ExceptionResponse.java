package com.mitocode.exception;
//Clase Pojo

import java.util.*;

public class ExceptionResponse{
    private Date timestamp;
    private String msg;
    private String detalles;

    public ExceptionResponse(Date timestamp,String msg,String detalles){
        this.timestamp = timestamp;
        this.msg = msg;
        this.detalles = detalles;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getDetalles() {
        return detalles;
    }

    public void setDetalles(String detalles) {
        this.detalles = detalles;
    }
    
}