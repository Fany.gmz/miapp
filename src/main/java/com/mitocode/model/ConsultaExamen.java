package com.mitocode.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

/**
 * ConsultaExamen
 * Tabla de muchos a muchos con java primaria compuesta
 * Deficion => se indica que son llaves, se agrupan emb
 */

 @Entity
 @IdClass(ConsultaExamenPK.class)//clase id representada
public class ConsultaExamen {

    @Id
    private Examen examen;

    @Id
    private Consulta consulta;

    public Examen getExamen() {
        return examen;
    }

    public void setExamen(Examen examen) {
        this.examen = examen;
    }

    public Consulta getConsulta() {
        return consulta;
    }

    public void setConsulta(Consulta consulta) {
        this.consulta = consulta;
    }    
}