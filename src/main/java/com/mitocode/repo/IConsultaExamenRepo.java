package com.mitocode.repo;

import com.mitocode.model.Consulta;
import com.mitocode.model.ConsultaExamen;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * IConsultaExamenRepo
 */
public interface IConsultaExamenRepo extends JpaRepository<Consulta,Integer>  {
    //Transaccional
    @Modifying
    @Query(value = "INSERT INTO consulta_examen(id_consulta,id_examen) VALUES(:idConsulta,:idExamen)",nativeQuery = true)
    Integer registrar(@Param("idConsulta") Integer idConsulta,@Param("idExamen") Integer idExamen); 
    
    @Query("FROM ConsultaExamen ce WHERE ce.consulta.idConsulta = :idConsulta")
    List<ConsultaExamen> listExamenesPorConsulta(@Param("idConsulta") Integer idConsulta);
}