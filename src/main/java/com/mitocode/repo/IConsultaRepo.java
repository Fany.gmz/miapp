package com.mitocode.repo;

import com.mitocode.model.Consulta;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * IConsultaRepo
 */
public interface IConsultaRepo extends JpaRepository<Consulta,Integer> {

  @Query("FROM Consulta c WHERE c.paciente.dui =:dui or LOWER(c.paciente.nombres) like %:nombreCompleto% or LOWER(c.paciente.apellidos) like %:nombreCompleto%")
  List<Consulta> buscar(@Param("dui") String dui,@Param("nombreCompleto") String nombreCompleto);
 
  @Query("FROM Consulta c where c.fecha between :fechaConsulta and :fechaSgte")
  List<Consulta> buscarFecha(@Param("fechaConsulta") LocalDateTime fechaConsulta,@Param("fechaSgte") LocalDateTime fechaSgte);
	
  @Query(value="CALL fn_listarResumen();",nativeQuery=true)
  List<Object[]> listarResumen();
  //Object Clase padre de java, crear una clase para manipularla facilemente
	
	
}