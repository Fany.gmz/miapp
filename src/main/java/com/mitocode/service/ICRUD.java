package com.mitocode.service;

import java.util.List;

//RECIBE UN GENERICO
public interface ICRUD<T>{
    T registrar(T t);
	T modificar(T t);
	List<T> listar();
	T leerPorId(Integer id);
	void eliminar(Integer id);
}