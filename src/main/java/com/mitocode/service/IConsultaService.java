package com.mitocode.service;
//Principal
import java.util.List;

import com.mitocode.dto.ConsultaListaExamenDTO;
import com.mitocode.dto.ConsultaResumenDTO;
import com.mitocode.dto.FiltroConsultaDTO;
import com.mitocode.model.Consulta;

/**
 * IConsultaService
 */
public interface IConsultaService  extends ICRUD<Consulta>{
    Consulta registrarTransaccional(ConsultaListaExamenDTO ConsultaDTO);
    List<Consulta> buscar(FiltroConsultaDTO filtro);
    List<Consulta> buscarFecha(FiltroConsultaDTO filtro);
    List<ConsultaResumenDTO> listarResumen();
    byte[] generarReporte();
}