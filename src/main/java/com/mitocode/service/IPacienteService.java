package com.mitocode.service;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.mitocode.model.Paciente;
//IPaciente Independiente
public interface IPacienteService extends ICRUD<Paciente> {
 Page<Paciente> listarPageable(Pageable pageable);
}
