package com.mitocode.service.impl;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mitocode.dto.ConsultaListaExamenDTO;
import com.mitocode.dto.ConsultaResumenDTO;
import com.mitocode.dto.FiltroConsultaDTO;
import com.mitocode.model.Consulta;
import com.mitocode.repo.IConsultaExamenRepo;
import com.mitocode.repo.IConsultaRepo;
import com.mitocode.service.IConsultaService;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class ConsultaServiceImpl implements IConsultaService {

	@Autowired	
	private IConsultaRepo repo;
	
	@Autowired
	private IConsultaExamenRepo cexamenRepo;

	@Override
	public Consulta registrar(Consulta obj) {
		obj.getDetalleConsulta().forEach(
			det ->{
				det.setConsulta(obj);
			}
		);
		return repo.save(obj);
	}

	@Override
	public Consulta modificar(Consulta obj) {
		return repo.save(obj);
	}
	@PreAuthorize("@restAuthServiceImpl.hasAccess('listar')")
	@Override
	public List<Consulta> listar() { 
		return repo.findAll();
	}

	@Override
	public Consulta leerPorId(Integer id) {
		return repo.findOne(id);
	}

	@Override
	public void eliminar(Integer id) {
		repo.delete(id);
	}
	@Transactional
	@Override
	public Consulta registrarTransaccional(ConsultaListaExamenDTO consultaDTO) {
		/* INSERT En Maestro detalle */
		consultaDTO.getConsulta().getDetalleConsulta().forEach(det -> det.setConsulta(consultaDTO.getConsulta()));
		repo.save(consultaDTO.getConsulta());
		/* INSERT En Tabla indepediente */
		consultaDTO.getListExamen().forEach(ex -> cexamenRepo.registrar(consultaDTO.getConsulta().getIdConsulta(),ex.getIdExamen()));
		return consultaDTO.getConsulta();
	}

	@Override
	public List<Consulta> buscar(FiltroConsultaDTO filtro) {
		return repo.buscar(filtro.getDui(), filtro.getNombreCompleto());
	}

	@Override
	public List<Consulta> buscarFecha(FiltroConsultaDTO filtro) {
		//localDatetime apartir java 8
		LocalDateTime fechaSgte = filtro.getFechaConsulta().plusDays(1);//Agregar un dia a la fecha almacenada.
		return repo.buscarFecha(filtro.getFechaConsulta(),fechaSgte);
	}

	@Override
	public List<ConsultaResumenDTO> listarResumen() {
	List<ConsultaResumenDTO> consulta = new ArrayList<>();
	repo.listarResumen().forEach(x ->{
		ConsultaResumenDTO cr = new ConsultaResumenDTO();
	     cr.setCantidad(Integer.parseInt(String.valueOf(x[0])));
	     cr.setFecha(String.valueOf(x[1]));
	     consulta.add(cr);
	});
		return consulta;
	}

	@Override
	public byte[] generarReporte(){
		byte[] data = null;
		File file;
		try {
			file = new ClassPathResource("/reports/consultas.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(),null,new JRBeanCollectionDataSource(this.listarResumen()));
			data = JasperExportManager.exportReportToPdf(print);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return data;
	}

}

















